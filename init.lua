-- Welcome to spears mod

dofile(core.get_modpath("spears").."/defaults.lua")
dofile(core.get_modpath("spears").."/functions.lua")
dofile(core.get_modpath("spears").."/tools.lua")

core.log("action", "[MOD] Spears loaded with throwing speed " .. (core.settings:get("spears_throw_speed")) .. " and drag coeff. " .. (core.settings:get("spears_drag_coeff")))
