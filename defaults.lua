-- Seems like defaults in settingtypes.txt are not taken by default, let's default them
if core.settings:get("spears_throw_speed") == nil then
	core.settings:set("spears_throw_speed", 13)
end

if core.settings:get("spears_drag_coeff") == nil then
	core.settings:set("spears_drag_coeff", 0.1)
end

if core.settings:get("spears_node_cracky_limit") == nil then
	core.settings:set("spears_node_cracky_limit", 3)
end

if core.settings:get("spears_full_punch_interval") == nil then
	core.settings:set("spears_full_punch_interval", 1.5)
	end

if core.settings:get("spears_enable_stone_spear") == nil then
	core.settings:set_bool("spears_enable_stone_spear", true)
end

if core.get_modpath("pigiron") then
	if core.settings:get("spears_enable_iron_spear") == nil then
		core.settings:set_bool("spears_enable_iron_spear", true)
	end
else
	core.settings:set_bool("spears_enable_iron_spear", false)
end

if core.settings:get("spears_enable_steel_spear") == nil then
	core.settings:set_bool("spears_enable_steel_spear", true)
end

if core.settings:get("spears_enable_copper_spear") == nil then
	core.settings:set_bool("spears_enable_copper_spear", true)
end

if core.settings:get("spears_enable_bronze_spear") == nil then
	core.settings:set_bool("spears_enable_bronze_spear", true)
end

if core.settings:get("spears_enable_obsidian_spear") == nil then
	core.settings:set_bool("spears_enable_obsidian_spear", true)
end

if core.settings:get("spears_enable_diamond_spear") == nil then
	core.settings:set_bool("spears_enable_diamond_spear", true)
end

if core.settings:get("spears_enable_gold_spear") == nil then
	core.settings:set_bool("spears_enable_gold_spear", true)
end
